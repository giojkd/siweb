

loadHomePage(false);

/*
 ****************************
 *On Page Callbacks
 ****************************
 */
pageCallbacks.home = function (page) {

}

pageCallbacks.home_scheduloo_mods = function (page) {
    var calendarDateFormat = app.calendar.create({
        inputEl: '#demo-calendar-date-format',
        dateFormat: 'M dd yyyy'
    });
}

pageCallbacks.register1_scheduloo = function (page) {
    var calendarDateFormat = app.calendar.create({
        inputEl: '#demo-calendar-date-format',
        dateFormat: 'M dd yyyy'
    });
}

function loadHomePage(animate) {
    mainView.router.load({
        url: 'home.html',
        context: {
        },
        animatePages: animate,
    });
}