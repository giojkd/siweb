// Let's register Template7 helper so we can pass json string in links
Template7.registerHelper('json_stringify', function (context) {
    return JSON.stringify(context);
});

var api_endpoint = 'http://localhost:8000/api';

var pageCallbacks = {};
// Initialize your app
var app = new Framework7({
    root: '#app',
    name: 'Scheduloo App',
    id: 'com.icteuro.scheduloo',
    // Add default routes
    routes: [

        {
            path:'/payment-methods/',
            url:'payment-methods.html'
        },
        {
            path:'/document-types/',
            url:'document-types.html'
        },
        {
            path:'/new-invoice-item/',
            url:'new-invoice-item.html'
        },
        {
            path:'/login/',
            url:'login-screen.html'
        },
        {
            path: '/edit-new-client/',
            url: 'edit-new-client.html',
        },
          {
            path: '/dashboard/',
            url: 'dashboard.html',
        },
         {
            path: '/clients-list/',
            url: 'clients-list.html',
        },

        {
            path: '/create-invoice/',
            url: 'create-invoice.html',
        },
        {
            path: '/list-invoice/',
            url: 'list-invoice.html',
        },
        {
            path: '/home-categories/',
            url: 'home-categories.html',
        },
        {
            path: '/register2-scheduloo/',
            url: 'register2-scheduloo.html',
        },
        {
            path: '/register3-scheduloo/',
            url: 'register3-scheduloo.html',
        },
        {
            path: '/page-scheduloo-cosa/',
            url: 'page-scheduloo-cosa.html',
        },
        {
            path: '/page-scheduloo-cosa-1/',
            url: 'page-scheduloo-cosa-1.html',
        },
        {
            path: '/home-scheduloo-mods/',
            url: 'home-scheduloo-mods.html',
        },
        {
            path: '/when-scheduloo/',
            url: 'when-scheduloo.html',
        },
        {
            path: '/results-scheduloo/',
            url: 'results-scheduloo.html',
        },
        {
            path: '/results-list/',
            url: 'results-list.html',
        },
        {
            path:'/product-page/:id/',
            url:'product-page.html',
        },
        {
            path:'/item-insert-form/',
            url:'item-insert-form.html'
        }
    ],
    on: {
        init: function (e) {
            //do something after app initialized
        },
        pageInit: function (page,id) {
            //do something during page initialized
            if(pageCallbacks[page.name]){
                pageCallbacks[page.name](page);
            }
        },
        pageBeforeIn: function (page) {
            //do something before page init

        },
        pageAfterIn: function (page) {
            //do something after page init
            switch(page.name){
                case 'product-page':
                  
                  var params = page.route.params;                  
                  var data = app.request.get(
                    api_endpoint+'/poi',
                    {id:params.id},
                    function(data){
                        var poi = data.data[0];
                        var map_product_center = poi;
                        var map_product = new google.maps.Map(document.getElementById('map-product'), {zoom: 12, center: map_product_center});
                        var map_product_marker = new google.maps.Marker({position: map_product_center, map: map_product});
                    },
                    function(data){},
                    'json');

                break;
                case 'results-list':
                var map_results_center = {lat:43.769562,lng:11.255814};
                var map_results =  new google.maps.Map(document.getElementById('map-results'), {zoom: 12, center: map_results_center});
                var map_results_marker = new google.maps.Marker({position: map_results_center, map: map_results});
                break;
            }
        },
        routerAjaxStart: function (xhr, options) {
            app.preloader.show();
        },
        routerAjaxComplete: function (xhr, options) {
            app.preloader.hide();
        }
    },
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = app.views.create('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: true,
});






