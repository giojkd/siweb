<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poi', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('administrative_area_level_1');
            $table->string('administrative_area_level_2');
            $table->string('administrative_area_level_3');
            $table->string('locality');
            $table->string('postal_code');
            $table->string('route');
            $table->string('street_number');
            $table->float('lat');
            $table->float('lng');
            $table->string('country');
            $table->string('place_id');
            $table->json('poi_json');
            $table->string('full_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poi');
    }
}
